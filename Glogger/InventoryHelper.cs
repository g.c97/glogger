﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Glogger
{
    class InventoryHelper
    {
        private List<ItemStruct> _items = new List<ItemStruct>();
        public InventoryHelper(string inventoryStreamBase64)
        {
            ZPackage pkg = new ZPackage(inventoryStreamBase64);
            Init(pkg);
        }

        public InventoryHelper(ZPackage pkg)
        {
            Init(pkg);
        }

        private void Init(ZPackage pkg)
        {
            int num = pkg.ReadInt();
            int num2 = pkg.ReadInt();
            for (int i = 0; i < num2; i++)
            {
                string text = pkg.ReadString();
                int stack = pkg.ReadInt();
                float durability = pkg.ReadSingle();
                VectorStruct pos = readVector(pkg.ReadVector2i());
                bool equiped = pkg.ReadBool();
                int quality = 1;
                if (num >= 101)
                {
                    quality = pkg.ReadInt();
                }
                int variant = 0;
                if (num >= 102)
                {
                    variant = pkg.ReadInt();
                }
                long crafterID = 0L;
                string crafterName = "";
                if (num >= 103)
                {
                    crafterID = pkg.ReadLong();
                    crafterName = pkg.ReadString();
                }
                if (text != "")
                {
                    this.AddItem(text, stack, durability, pos, equiped, quality, variant, crafterID, crafterName);
                }
            }
        }


        private void AddItem(string name, int amount, float durability, VectorStruct pos, bool equiped, int quality, int variant, long crafterID, string crafterName)
        {
            ItemStruct item = new ItemStruct
            {
                name = name,
                amount = amount,
                durability = durability,
                pos = pos,
                equiped = equiped,
                quality = quality,
                variant = variant,
                crafterID = crafterID,
                crafterName = crafterName
            };
            ItemStruct existingItem = _items.Where(i => i.name == name && Math.Round(i.durability) == Math.Round(durability) && i.quality == quality && i.variant == variant && i.crafterID == crafterID && i.crafterName == crafterName).FirstOrDefault();
            if (existingItem != null)
            {
                existingItem.amount += amount;
                return;
            }
            _items.Add(item);
        }

        public string GetJSON()
        {
            List<string> arr = new List<string>();
            foreach (ItemStruct item in _items)
            {
                arr.Add(JsonUtility.ToJson(item));
            }
            string json = "{\"items\": [" + string.Join(", ", arr.ToArray()) + "]}";
            return json;
        }

        public string Diff(string oldInventory)
        {
            // TODO: match strings for optimization
            Logger.Log("Diff called. OldInventory Json = " + oldInventory);
            Logger.Log("Current Inventory JSON (new): " + GetJSON());
            List<ItemStruct> oldItems = new List<ItemStruct>();
            List<ItemStruct> itemsAdded = new List<ItemStruct>();
            List<ItemStruct> itemsRemoved = new List<ItemStruct>();
            SimpleJSON.JSONNode oldInventoryObject = SimpleJSON.JSON.Parse(oldInventory);
            List<string> itemsAddedJSON = new List<string>();
            List<string> itemsRemovedJSON = new List<string>();

            foreach (SimpleJSON.JSONNode oldItemJSON in oldInventoryObject["items"])
            {
                Logger.Log("oldItemJSON.toString(): " + oldItemJSON.ToString());
                if (oldItemJSON["name"].Value.Length == 0) continue;
                ItemStruct oldItem = new ItemStruct
                {
                    name = oldItemJSON["name"].Value,
                    amount = Int32.Parse(oldItemJSON["amount"].Value),
                    durability = (float)Convert.ToDouble(oldItemJSON["durability"].Value),
                    quality = Int32.Parse(oldItemJSON["quality"].Value),
                    variant = Int32.Parse(oldItemJSON["variant"].Value),
                    crafterID = (long)Convert.ToDouble(oldItemJSON["crafterID"].Value),
                    crafterName = oldItemJSON["crafterName"].Value
                };
                Logger.Log("oldItem: " + oldItem);
                oldItems.Add(oldItem);
                ItemStruct newItem = _items.Where(i => i.name == oldItem.name && Math.Round(i.durability) == Math.Round(oldItem.durability) && i.quality == oldItem.quality && i.variant == oldItem.variant && i.crafterID == oldItem.crafterID && i.crafterName == oldItem.crafterName).FirstOrDefault();
                if (newItem == null)
                {
                    Logger.Log("Item not found in new storage inventory. Therefore removed: " + oldItem);
                    itemsRemoved.Add(oldItem);
                    continue;
                }
                Logger.Log("newItem: " + JsonUtility.ToJson(newItem));
                Logger.Log("oldItem: " + JsonUtility.ToJson(oldItem));
                Logger.Log("newItem.amount: " + newItem.amount);
                Logger.Log("oldItem.amount: " + oldItem.amount);
                Logger.Log("Amount: " + (newItem.amount == oldItem.amount));
                Logger.Log("Durability: " + (newItem.durability == oldItem.durability));
                Logger.Log("Quality: " + (newItem.quality == oldItem.quality));
                Logger.Log("Variant: " + (newItem.variant == oldItem.variant));
                Logger.Log("CrafterID: " + (newItem.crafterID == oldItem.crafterID));
                Logger.Log("CrafterName: " + (newItem.crafterName == oldItem.crafterName));
                Logger.Log(oldItem.name + " checked for existing: " + newItem);
                // Item changed
                if (newItem.amount > oldItem.amount)
                {
                    ItemStruct tmpItem = copyItem(oldItem);
                    tmpItem.amount = newItem.amount - oldItem.amount;
                    Logger.Log("newItem.amount > oldItem.amount therefore amount increased. Added item: " + tmpItem);
                    itemsAdded.Add(tmpItem);
                }
                else if (newItem.amount < oldItem.amount)
                {
                    ItemStruct tmpItem = copyItem(oldItem);
                    tmpItem.amount = oldItem.amount - newItem.amount;
                    Logger.Log("newItem.amount < oldItem.amount therefore amount decreased. Removed item: " + tmpItem);
                    itemsRemoved.Add(tmpItem);
                }
            }
            foreach (ItemStruct item in _items)
            {
                if (item is null) continue;
                //
                ItemStruct itemExisted = oldItems.Where(i => i.name == item.name && Math.Round(i.durability) == Math.Round(item.durability) && i.quality == item.quality && i.variant == item.variant && i.crafterID == item.crafterID && i.crafterName == item.crafterName).FirstOrDefault();
                if (itemExisted == null)
                {
                    Logger.Log("Old item not found, therefore new has been added. New item: " + item);
                    itemsAdded.Add(item);
                }
            }

            foreach (ItemStruct item in itemsAdded)
            {
                String json = JsonUtility.ToJson(item);
                itemsAddedJSON.Add(json);
                Logger.Log("Final. Added item: " + json);
            }
            foreach (ItemStruct item in itemsRemoved)
            {
                String json = JsonUtility.ToJson(item);
                itemsRemovedJSON.Add(json);
                Logger.Log("Final. Removed item: " + json);
            }
            Logger.Log("Diff finished.");
            return "{\"itemsAdded\": [" + string.Join(", ", itemsAddedJSON.ToArray()) + "], \"itemsRemoved\": {[" + string.Join(", ", itemsRemovedJSON.ToArray()) + "]}}";
        }

        private VectorStruct readVector(Vector2i vec)
        {
            VectorStruct ret = new VectorStruct();
            ret.x = vec.x;
            ret.y = vec.y;
            return ret;
        }

        [Serializable]
        private class VectorStruct
        {
            public int x;
            public int y;
        }

        [Serializable]
        private class ItemStruct
        {
            public string name;
            public int amount;
            public float durability;
            public VectorStruct pos;
            public bool equiped;
            public int quality;
            public int variant;
            public long crafterID;
            public string crafterName;
        }

        private ItemStruct copyItem(ItemStruct item)
        {
            return new ItemStruct
            {
                name = item.name,
                amount = item.amount,
                durability = item.durability,
                pos = item.pos,
                equiped = item.equiped,
                quality = item.quality,
                variant = item.variant,
                crafterID = item.crafterID,
                crafterName = item.crafterName
            };
        }

        private class Logger
        {
            private static bool _DEBUG = true;
            public static void Log(string message)
            {
                if (_DEBUG) Debug.Log(message);
            }
        }
    }
}
