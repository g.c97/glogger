﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Glogger
{
    public class DataService
    {

        private SQLiteConnection _connection;

        public DataService(string DatabaseName)
        {
            // check if file exists in Application.persistentDataPath
            String filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

            if (!File.Exists(filepath))
            {
                Debug.Log("Database not in Persistent path");
                String loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;
                if (File.Exists(loadDb)) File.Copy(loadDb, filepath);
                Debug.Log("Database written");
            }

            String dbPath = filepath;

            _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
            Debug.Log("Final PATH: " + dbPath);
            CreateDB();
        }

        public void CreateDB()
        {
            // Temporary measure for test purposes (delete old table). WARNING: TURN OFF ON RELEASE
            _connection.DropTable<StorageLog>();
            _connection.CreateTable<StorageLog>();
        }

        public IEnumerable<StorageLog> GetStorageLogs() => _connection.Table<StorageLog>();

        public StorageLog getPreviousLog(uint storage_id) => _connection.Table<StorageLog>().OrderByDescending(x => x.id).Where(x => x.storage_id == storage_id).FirstOrDefault();

        public void CreateStorageLog(string user_steam_id, uint storage_id, string raw_content)
        {
            String timestamp = DateTime.UtcNow.ToString("s", System.Globalization.CultureInfo.InvariantCulture);
            InventoryHelper inventory = parse_items(raw_content);
            StorageLog previousLog = getPreviousLog(storage_id);
            string diff = inventory.Diff(previousLog != null ? previousLog.unique_items : "{\"items\": []}");
            bool hasDiff = diff != "{\"itemsAdded\": [], \"itemsRemoved\": {[]}}";

            if (!hasDiff && !Glogger.writeAccess.Value)
            {   
                return;
            }

            StorageLog log = new StorageLog
            {
                user_steam_id = user_steam_id,
                storage_id = storage_id,
                raw_content = raw_content,
                timestamp = timestamp,
                unique_items = inventory.GetJSON(),
                diff = diff,
                hasDiff = hasDiff
            };
            _connection.Insert(log);
        }

        private static InventoryHelper parse_items(string raw_content)
        {
            InventoryHelper ih = new InventoryHelper(raw_content);
            return ih;
        }
    }
}
