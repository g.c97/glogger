﻿namespace Glogger
{
    public class StorageLog
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string user_steam_id { get; set; }
        public uint storage_id { get; set; }
        public string raw_content { get; set; }
        public string timestamp { get; set; }
        public string unique_items { get; set; }
        public string diff { get; set; }
        public bool hasDiff { get; set; }

        /*CREATE TABLE RawChests(
			id integer PRIMARY KEY AUTOINCREMENT, // id
			uid integer, // user id (caller)
			chest_id integer, // id of chest
			raw_content string, // new content of chest in base64
			time timestamp, // timestamp of action
			unique_items text, // unique items amount in the chest in JSON format
			diff text // difference between unique items of this and previous request
		);*/


    }
}
