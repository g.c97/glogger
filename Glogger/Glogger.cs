﻿using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;



namespace Glogger
{
    [BepInPlugin(ModGUID, ModName, ModVersion)]
    public class Glogger : BaseUnityPlugin
    {
        private const string ModName = "Glogger";
        private const string ModVersion = "0.3.0.0";
        private const string ModGUID = "frej.valheim.glogger";

        private static Harmony harmony = null!;
        private static DataService dataService = new DataService("storage_logs");
        private static readonly string[] _CHEST_PREFABS = { "Chest", "piece_chest", "piece_chest_wood", "piece_chest_blackmetal", "piece_chest_private", "piece_chest_treasure", "piece_chest_wood", "piece_gift1", "piece_gift2", "piece_gift3", "Player_tombstone", "shipwreck_karve_chest", "stonechest", "TreasureChest_blackforest", "TreasureChest_fCrypt", "TreasureChest_forestcrypt", "TreasureChest_heath", "TreasureChest_meadows", "TreasureChest_meadows_buried", "TreasureChest_mountains", "TreasureChest_plains_stone", "TreasureChest_sunkencrypt", "TreasureChest_swamp", "TreasureChest_trollcave", "CargoCrate", "loot_chest_stone", "loot_chest_wood" };

        private static ConfigEntry<bool> _isModEnabled;
        public static ConfigEntry<bool> writeAccess;

        // TODO: new thread

        public void Awake()
        {
            _isModEnabled = Config.Bind("Global", "isModEnabled", true, "Globally enable or disable this mod.");
            if (!_isModEnabled.Value)
            {
                Debug.Log("[" + ModName + "] Mod is disabled in config.");
                return;
            }
            writeAccess =
            Config.Bind(
                "LogLevel",
                "writeAccessToStorage",
                false,
                "Writes access to storage (when no actual change is detected)");
            Assembly assembly = Assembly.GetExecutingAssembly();
            harmony = new(ModGUID);
            harmony.PatchAll(assembly);
        }


        [HarmonyPatch(typeof(ZRoutedRpc))]
        class ZRoutedRPCLogger
        {
            [HarmonyPostfix]
            [HarmonyPatch(typeof(ZRoutedRpc), nameof(ZRoutedRpc.HandleRoutedRPC))]
            static void HandleRoutedRPCLogger(ZRoutedRpc.RoutedRPCData data)
            {
                if (data.m_methodHash == "RequestOpen".GetStableHashCode())
                {
                    Debug.Log("RequestOpen");
                }
            }

            [HarmonyPostfix]
            [HarmonyPatch(typeof(ZRoutedRpc), nameof(ZRoutedRpc.RPC_RoutedRPC))]
            static void RouteddRPCLogger(ZRpc rpc, ZPackage pkg)
            {

            }
        }

        [HarmonyPatch(typeof(ZDOMan)/*, nameof(ZDOMan.RPC_ZDOData)*/)]
        class ZDOManLogger
        {
            private static prefabHelper chestChecker = new prefabHelper(_CHEST_PREFABS);
            [HarmonyTranspiler]
            [HarmonyPatch(nameof(ZDOMan.RPC_ZDOData))]
            static IEnumerable<CodeInstruction> RPC_ZDODataLogger(IEnumerable<CodeInstruction> instructions)
            {
                return new CodeMatcher(instructions)
                    .MatchEndForward(
                            /*
                             Injection place:
                                124	0143	ldloc.s	V_13 (13)
                                125	0145	ldloc.3
                                126	0146	callvirt	instance void ZDO::Deserialize(class ZPackage)
                                // here
                                127	014B	call	class ZNet ZNet::get_instance()
                                128	0150	callvirt	instance bool ZNet::IsServer()
                                129	0155	brfalse	39 (005A) ldarg.2 
                             */
                            new CodeMatch(OpCodes.Callvirt, AccessTools.Method(typeof(ZDO), nameof(ZDO.Deserialize)))
                        )
                    .Insert(
                    new CodeInstruction(OpCodes.Ldloc_S, (byte)13),
                    new CodeInstruction(OpCodes.Ldloc_0),
                    new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(ZDOManLogger), nameof(ZDOManLogger.inject)))
                    )
                    .InstructionEnumeration();
            }

            /*
                        static bool Prefix(ZRpc rpc, ZPackage pkg)
                        {
                            ZDOMan.ZDOPeer zdopeer = ZDOMan.instance.FindPeer(rpc);
                            if (zdopeer == null)
                            {
                                ZLog.Log("ZDO data from unkown host, ignoring");
                                return false;
                            }
                            float time = Time.time;
                            int num = 0;
                            ZPackage pkg2 = new ZPackage();
                            int num2 = pkg.ReadInt();
                            for (int i = 0; i < num2; i++)
                            {
                                ZDOID id = pkg.ReadZDOID();
                                ZDO zdo = ZDOMan.instance.GetZDO(id);
                                if (zdo != null)
                                {
                                    zdo.InvalidateSector();
                                }
                            }
                            for (; ; )
                            {
                                ZDOID zdoid = pkg.ReadZDOID();
                                if (zdoid.IsNone())
                                {
                                    break;
                                }
                                num++;
                                uint num3 = pkg.ReadUInt();
                                uint num4 = pkg.ReadUInt();
                                long owner = pkg.ReadLong();
                                Vector3 vector = pkg.ReadVector3();
                                pkg.ReadPackage(ref pkg2);
                                ZDO zdo2 = ZDOMan.instance.GetZDO(zdoid);
                                bool flag = false;
                                if (zdo2 != null)
                                {
                                    if (num4 <= zdo2.m_dataRevision)
                                    {
                                        if (num3 > zdo2.m_ownerRevision)
                                        {
                                            zdo2.m_owner = owner;
                                            zdo2.m_ownerRevision = num3;
                                            zdopeer.m_zdos[zdoid] = new ZDOMan.ZDOPeer.PeerZDOInfo(num4, num3, time);
                                            continue;
                                        }
                                        continue;
                                    }
                                }
                                else
                                {
                                    zdo2 = ZDOMan.instance.CreateNewZDO(zdoid, vector);
                                    flag = true;
                                }
                                zdo2.m_ownerRevision = num3;
                                zdo2.m_dataRevision = num4;
                                zdo2.m_owner = owner;
                                zdo2.InternalSetPosition(vector);
                                zdopeer.m_zdos[zdoid] = new ZDOMan.ZDOPeer.PeerZDOInfo(zdo2.m_dataRevision, zdo2.m_ownerRevision, time);
                                zdo2.Deserialize(pkg2);
                                if (ZNet.instance.IsServer() && flag && ZDOMan.instance.m_deadZDOs.ContainsKey(zdoid))
                                {
                                    zdo2.SetOwner(ZDOMan.instance.m_myid);
                                    ZDOMan.instance.DestroyZDO(zdo2);
                                }

                                // my code
                                inject(zdo2, zdopeer);
                                // /my code
                            }
                            ZDOMan.instance.m_zdosRecv += num;
                            return false;
                        }
            */
            static void inject(ZDO zdo, ZDOMan.ZDOPeer zdopeer)
            {
                if (chestChecker.isPrefabInGroup(zdo.m_prefab))
                {
                    Debug.Log("Prefab: " + zdo.m_prefab);
                    string b64Items = zdo.GetString("items", "");
                    if (!b64Items.IsNullOrWhiteSpace()) // ignore fresh chests, they don't have "items" variable
                    {
                        ZPackage itemsPkg = new ZPackage(b64Items);
                        Debug.Log("Chest ID: " + zdo.m_uid.id);
                        List<ZNet.PlayerInfo> playerList = ZNet.instance.GetPlayerList();
                        string nickname = "";
                        string steamID = "";
                        ZNet.PlayerInfo player = playerList.Find(x => x.m_name == zdopeer.m_peer.m_playerName);
                        nickname = player.m_name;
                        steamID = player.m_host;
                        dataService.CreateStorageLog(steamID, zdo.m_uid.id, b64Items);
                    }
                }
            }


            class prefabHelper
            {
                HashSet<int> _prefabList = new HashSet<int>();

                public prefabHelper(String[] group)
                {
                    generatePrefabList(group);
                }

                private void generatePrefabList(String[] group)
                {
                    foreach (string validPrefabName in group)
                    {
                        _prefabList.Add(validPrefabName.GetStableHashCode());
                    }
                }

                public bool isPrefabInGroup(int prefab)
                {
                    return _prefabList.Contains(prefab);
                }
            }
        }



        public void OnDestroy()
        {
            harmony.UnpatchSelf();
        }
    }
}
